local luadir   = "../lua-5.1/src"
local gmbindir = "E:/Steam/steamapps/common/GarrysMod/garrysmod/bin"
local osstring = os.get()

solution "gm_fgtmenu"

	language "C++"
	platforms { "x32" }
	location ( osstring .."-".. _ACTION )
	flags { "Symbols", "NoEditAndContinue", "NoPCH", "StaticRuntime", "EnableSSE" }
	targetdir ( "bin/" .. osstring .. "/" )
	configurations { "" }

	includedirs {
		luadir,
		"./lib"
	}

	buildoptions { "-std=gnu++11" }
	files { "src/**.*", "lib/*.cpp" }
	kind "SharedLib"

if ( osstring == "windows" ) then

	project "gm_fgtmenu"
		defines { "WIN32", "GMMODULE" }
		linkoptions  { gmbindir .. "/lua_shared.dll" }

elseif ( osstring == "linux" ) then
	
	project "gm_fgtmenu"
		defines { "LINUX", "GMMODULE" }
		linkoptions  { gmbindir .. "/lua_shared.so" }

else

	Error( "Platform not supported." )

end
