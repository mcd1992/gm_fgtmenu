#if defined( WIN32 )

	#undef _UNICODE
	#define GLUA_DLL_EXPORT __declspec( dllexport )
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>

#elif defined( LINUX )

	#define GLUA_DLL_EXPORT __attribute__((visibility("default"))) // TODO: Lots of linux changes to make

#endif

#define CLIENT 0
#define SERVER 1
#define MENU   2
#define CREATELUAINTERFACE 4
#define CLOSELUAINTERFACE  5
#define LOADFILEINDEX 7
#define REQUIREINDEX 120
#define INITLUA_PATH "includes\\init.lua"

extern "C" {
#include "lua.h"
}

#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <cstring>
#include <string>
#include "lua_shared.h"
#include "lua_getinterface.h"
#include "vptr.h"
#include "vtable.h"

static LuaShared *lua_shared      = NULL;
static LuaInterface *client_state = NULL;
static lua_State *menu_state      = NULL;
static VPTR *sharedhook           = NULL;
static VPTR *clienthook           = NULL;

typedef void* (*MsgType)(const char* msg, ...); // Function prototype for tier0's Msg()
static MsgType Msg = NULL;

typedef void* (__thiscall *LoadFileType)(LuaShared*, const char**, const char*, bool, bool);
void* __fastcall myLoadFile( LuaShared *_this, void *ignore, const char **filename, const char *realm, bool unk, bool allwaystrue ) {

	const char *fname = *filename;
	if ( strcmp( fname, INITLUA_PATH ) == 0 ) {
		Msg( "%s is being loaded. Calling FgtPreInitLua in menu state.\n", INITLUA_PATH );

		lua_getglobal( menu_state, "FgtPreInitLua" );
		lua_call( menu_state, 0, 0 );

		sharedhook->Unhook( LOADFILEINDEX );
	}

	//Msg( "LoadFile( %s, %s, %s, %s )\n\n\n", fname, realm, unk ? "true" : "false", allwaystrue ? "true" : "false" );
	return sharedhook->GetOriginal<LoadFileType>(LOADFILEINDEX)(_this, filename, realm, unk, allwaystrue);
}

typedef LuaInterface* (__thiscall *CreateLuaInterfaceType)(LuaShared*, unsigned char, bool);
void* __fastcall myCreateLuaInterface( LuaShared *_this, void *ignore, unsigned char state, bool renew ) {
	LuaInterface *newstate = sharedhook->GetOriginal<CreateLuaInterfaceType>(CREATELUAINTERFACE)(_this, state, renew);

	if ( state == CLIENT ) {
		Msg( "CreateLuaInterface called for client interface\n" );
		client_state = newstate;

		if ( clienthook ) {
			Msg( "Removing old clienthook to hook new one\n" );
			clienthook->Remove();
		}
		clienthook = new VPTR( client_state );
		//clienthook->Hook( REQUIREINDEX, (void*)myRequire );

		sharedhook->Hook( LOADFILEINDEX, (void*)myLoadFile );
	}

	return newstate;
}

typedef void* (__thiscall *CloseLuaInterfaceType)(LuaShared*, LuaInterface*);
void* __fastcall myCloseLuaInterface( LuaShared *_this, void *ignore, LuaInterface* iface ) {
	if ( iface == client_state ) {
		Msg( "CloseLuaInterface called on client interface.\n" );
		client_state = NULL;
	}

	return sharedhook->GetOriginal<CloseLuaInterfaceType>(CLOSELUAINTERFACE)(_this, iface);
}

int Lua_RunOnClient( lua_State *state ) {
	if ( !client_state ) {
		Msg( "Client state invalid!\n" );
		return 0;
	}

	if ( lua_isstring( state, 1 ) ) {
		const char *runstring = lua_tostring( state, 1 );
		client_state->RunStringEx( "lua/includes/init.lua", "", runstring, true, true, true );
	}

	return 0;
}

extern "C" GLUA_DLL_EXPORT int gmod13_open( lua_State *L ) {
	menu_state = L;

	HMODULE tier0 = GetModuleHandle( "tier0.dll" ); // TODO: Linux/osx
	if ( tier0 != NULL ) {
		Msg = (MsgType)GetProcAddress( tier0, "Msg" );
	}

	lua_shared = GetSharedLuaInterface();
	if ( lua_shared != NULL ) {
		sharedhook = new VPTR( lua_shared );
		sharedhook->Hook( CREATELUAINTERFACE, (void*)myCreateLuaInterface );
		sharedhook->Hook( CLOSELUAINTERFACE, (void*)myCloseLuaInterface );
	}

	lua_pushcfunction( L, Lua_RunOnClient );
	lua_setglobal( L, "RunOnClient" );

	Msg( "gm_fgtmenu loaded.\n" );
	return 0;
}

extern "C" GLUA_DLL_EXPORT int gmod13_close( lua_State *L ) {
	menu_state = NULL;

	if ( sharedhook != NULL ) {
		sharedhook->Remove();
		sharedhook = NULL;
	}
	if ( clienthook != NULL ) {
		clienthook->Remove();
		clienthook = NULL;
	}

	Msg( "gm_fgtmenu unloaded.\n" );
	return 0;
}
