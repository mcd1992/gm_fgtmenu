#ifndef _GET_INTERFACE_H
#define _GET_INTERFACE_H

#include "lua_shared.h"

LuaShared* GetSharedLuaInterface();

#endif
