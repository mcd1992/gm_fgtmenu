#ifdef WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include "lua_getinterface.h"

//
// This function will return the LuaShared interface from lua_shared.dll
//
LuaShared* GetSharedLuaInterface() {
	typedef void* (*CreateInterfaceType)(const char* interface_name, int return_code); // Function prototype for lua_shared's CreateInterface

	HMODULE luadll = GetModuleHandle( "lua_shared.dll" ); // TODO: Linux/osx
	if ( luadll == NULL ) {
		return NULL;
	}

	CreateInterfaceType CreateInterface = (CreateInterfaceType)GetProcAddress( luadll, "CreateInterface" );
	if ( CreateInterface == NULL ) {
		return NULL;
	}

	void *ret = CreateInterface( "LUASHARED003", 0 );
	return (LuaShared*)ret;
};
