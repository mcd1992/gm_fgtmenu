#ifndef _VTABLE_H
#define _VTABLE_H

#include <cstdlib>

void SetVTable(void *object, void *VTable);
void **GetVTable(void *object);
void *GetMethod(void *object, int offset);

size_t GetVTableSize(void **VTable);

#endif
